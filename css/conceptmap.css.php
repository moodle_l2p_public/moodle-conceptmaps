<?php
	header("Content-Type: text/css");
	require(__DIR__.'/../../../config.php');
	require_once(__DIR__.'/../lib.php');
	require_once(__DIR__.'/../locallib.php');
?>
.conceptmap {
    /* for IE10+ touch devices */
    touch-action:none;
    height: 960px;
    position: absolute;
    width: 1440px;
    background-color: white;
}

.afterconceptmap {
    margin-top: 990px;
}


/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
        @media screen and (max-width: 1200px) {
            .conceptmap {
                /* for IE10+ touch devices */
                touch-action:none;
                height: 800px;
                position: absolute;
                width: 1200px;
                background-color: white;
                marginLeft = "0";
            }
        }
.w {
    padding: 0px;
    padding-left: 0px;
    padding-right: 25px;
    position: absolute;
    z-index: 4;
    width: 13em;
    border: 1px solid #2e6f9a;
    box-shadow: 2px 2px 19px #e0e0e0;
    -o-box-shadow: 2px 2px 19px #e0e0e0;
    -webkit-box-shadow: 2px 2px 19px #e0e0e0;
    -moz-box-shadow: 2px 2px 19px #e0e0e0;
    -moz-border-radius: 8px;
    border-radius: 8px;
    opacity: 0.8;
    filter: alpha(opacity=80);
    cursor: move;
    background-color: white;
    font-size: 11px;
    -webkit-transition: background-color 0.25s ease-in;
    -moz-transition: background-color 0.25s ease-in;
    transition: background-color 0.25s ease-in;
    white-space:pre-line;
}

.w:hover {
    /*background-color: #5c96bc;*/
    /*color: white;*/

}

.w-drag-box {
    position: absolute;
    left: 0px;
    top: 0px;
    bottom: 0px;
    right: 40px;
}

.aLabel {
    -webkit-transition: background-color 0.25s ease-in;
    -moz-transition: background-color 0.25s ease-in;
    transition: background-color 0.25s ease-in;
}

.aLabel._jsPlumb_hover, ._jsPlumb_source_hover, ._jsPlumb_target_hover {
    /*background-color: #1e8151;*/
    /*color: white;*/
}

.aLabel {
    background-color: white;
    opacity: 0.8;
    padding: 0.3em;
    border-radius: 0.5em;
    border: 1px solid #346789;
    cursor: pointer;
}

.editable-node {
	cursor: pointer;
}

.ep {
    position: absolute;
    top: 1.5px;
    bottom: 1.5px;
    right: 3px;
    width: 25px;
    background-color: orange;
    cursor: pointer;
    box-shadow: 0 0 1.5px black;
    -webkit-transition: -webkit-box-shadow 0.25s ease-in;
    -moz-transition: -moz-box-shadow 0.25s ease-in;
    transition: box-shadow 0.25s ease-in;
}

.ep:hover {
    box-shadow: 0px 0px 6px black;
}

.conceptmap ._jsPlumb_endpoint {
    z-index: 3;
}

<?php

    //hier wird das CSS fuer jeden einzelnen Begriff erstellt, da jeder eine Verschiedene hoehe hat, je nach dem, wie viel Text der Begriff hat.
    $topicid = required_param('topic', PARAM_INT);
		$topic = get_topic($topicid);
		$i = 0;
		foreach ($topic->terms as $key => $term) {
				$height = strlen($term->name) <= 25? "32" : "48";
								print("#state{$term->id} {\n");
								print("\tleft: 10px;\n");
								print("\theight: {$height}px;\n");
								print("\tline-height: " . ($height) . "px;\n");
								print("\ttop: " . ($i * ($height+2)) . "px;\n");
								print("}\n");
								print("#state{$term->id} span { line-height: 12px; vertical-align: middle; display: inline-block; }\n");
								$i++;
				}

/*
    if(isset($_GET['user'])){
        $userID = filter_input(INPUT_GET, 'user', FILTER_VALIDATE_INT);
        $user = Student::getStudentByID($userID);
        $options=array('options'=>array('default'=>1, 'min_range'=>1));
        $version = filter_input(INPUT_GET, 'version', FILTER_VALIDATE_INT, $options);
        $stu_begriffe = StuBegriffe::getBegriffeVersion($thema, $user, $version);
        $begriffeTU[$thema_id][$userID] = $stu_begriffe;
        $anzahlIDs = count($begriffeTU[$thema_id][$userID]);
            for ($i = 0; $i < $anzahlIDs; $i++) {
                    print("#state{$begriffeTU[$thema_id][$userID][$i]->getID()} {\n");
                    print("\tleft: 10px;\n");
                    print("\theight: {$css["height"]}px;\n");
                    print("\tline-height: " . ($css["height"]) . "px;\n");
                    print("\ttop: " . ($i * ($css["height"]+2)) . "px;\n");
                    print("}\n");
                    print("#state{$begriffeTU[$thema_id][$userID][$i]->getID()} span { line-height: 12px; vertical-align: middle; display: inline-block; }\n");
            }
    }else{
        $anzahlIDs = count($themen[$thema_id]);
        for ($i = 0; $i < $anzahlIDs; $i++) {
                    print("#state{$themen[$thema_id][$i]->getID()} {\n");
                    print("\tleft: 10px;\n");
                    print("\theight: {$css["height"]}px;\n");
                    print("\tline-height: " . ($css["height"]) . "px;\n");
                    print("\ttop: " . ($i * ($css["height"]+2)) . "px;\n");
                    print("}\n");
                    print("#state{$themen[$thema_id][$i]->getID()} span { line-height: 12px; vertical-align: middle; display: inline-block; }\n");
            }
    }*/

?>

.dragHover {
    border: 2px solid orange;
}

path, ._jsPlumb_endpoint { cursor:pointer; }

.modal-body ul li {
	list-style-type: disc;
	margin-left: 15px;
}

.disabledbutton {
    pointer-events: none;
}

.conecptMapForMe {
    background-color: red;
}

.tooltipKorrektur {
    position: relative;
    display: inline-block;
}

.tooltipKorrektur .tooltipKorrekturInhalt {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -60px;
}

.tooltipKorrektur .tooltipKorrekturInhalt.tooltipKorrekturInhaltShow {
    visibility: visible;
}

.tooltipKorrektur .tooltipKorrekturInhalt::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: black transparent transparent transparent;
}

._jsPlumb_hover .tooltipKorrekturInhalt {
    visibility: visible;
}


.studentContainerKorrektur {
    padding-left: 45px;
    margin-left: 0 !important;
}

.navbarlinkdisabled{
    cursor: default;
    background-image: none;
    opacity: 0.65;
    color: #ffffff;
    background-color: #00549f;
    pointer-events:none;
}

.x2 {
    transform: scale(2, 2);
}
