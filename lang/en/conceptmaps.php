<?php
# @Date:   2019-11-05T11:40:53+01:00
# @Last modified time: 2020-03-10T15:24:16+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_conceptmaps
 * @category    string
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Concept Maps';
$string['modulename'] = 'Concept Maps';
$string['modulenameplural'] = 'Concept Maps';
$string['pluginadministration'] = 'Concept Maps administration';

$string['conceptmapsname'] = 'Name of Conceptmap tool';
$string['conceptmapssettings'] = 'Settings of Conceptmaps instance';
$string['conceptmapsfieldset'] = 'Fieldset of Conceptmaps instance';
$string['conceptmapsname_help'] = 'Help';

$string['overview'] = 'Overview';
$string['topics_overview'] = 'Topics';
$string['submissions'] = 'Submissions';
$string['autocorrection'] = 'Autocorrection';
$string['groups'] = 'Groups';
$string['help'] = 'Help';

$string['strftimedate'] = '%d %b %y, %I:%M %p';

$string['noTopicsAvailable'] = 'No topics available at the moment!';
$string['createTopic'] = 'Create new topic';
$string['topic'] = "Topic";
$string['topic_name'] = 'Name of Topic';
$string['header_date'] = 'Date settings';
$string['topic_start'] = 'Start';
$string['topic_end'] = 'End';
$string['topic_startcorrection'] = 'Start of Correction';
$string['topic_endcorrection'] = 'End of Correction';

$string['header_terms'] = 'Terms';
$string['topic_term'] = 'Term {$a}';

$string['topic_create_submit'] = 'Create';
$string['topic_edit_submit'] = 'Save';
$string['topic_term_empty_count'] = 'Number of empty terms';
$string['store_successful'] = 'Creating was successful';
$string['edit'] = 'Edit';
$string['view'] = 'View';

$string['save_successful'] = 'Saved successfully';

$string['conceptmap_saved_success'] = 'Saved conceptmap successfully.';
$string['conceptmap_saved_error'] = 'Error while saving conceptmap';
$string['zoomPlus'] = 'Zoom in';
$string['zoomMinus'] = 'Zoom out';


$string['editconceptmapterm'] = "Edit term";
$string['newtermfor'] = 'New name for {$a}';
$string['conceptmap_saved_error'] = "Error occured during saving the conceptmap";
$string['conceptmap_saved_success'] = "Conceptmap saved";
$string['submittingConceptmap'] = "Submit Conceptmap";
$string['confirmSubmitting'] = "Are you sure to submit the conceptmap? Afterwards you cannot edit your conceptmap anymore.";
$string['yesButton'] = "Yes";
$string['cancelButton'] = "Cancel";
$string['SaveAndBack'] = "Save and Back to Overview";
$string['confirmSaveAndBack'] = "Are you sure to leave? This only saves the conceptmap. To submit it, use the \"Submit\" button.";
$string['topic_count_empty_terms_notnumeric'] = "The number of empty terms must be a number";
$string['topic_name_missing'] = "A name for this topic is required";
$string['submissionFor'] = 'Submission for {$a}';
$string['user'] = "User";
$string['version'] = "Version";
$string['save'] = "Save";
$string['submit'] = "Submit";
$string['redoCheckbox'] = "Mark for redo";
$string['correctedCheckbox'] = "Mark as corrected";
$string['correction'] = "Correction";
$string['singleEdges'] = "Single Edges";
$string['conceptmap_auto_edge_already_exists'] = "This auto edge does already exist";
$string['conceptmap_change_verification_failed'] = "Changing the verification failed";
$string['toUnverified'] = "To unverified edges";
$string['toVerified'] = "To verified edges";
$string['status'] = "Status";
$string['correctionOfSingleConnections'] = "Correction of Single Connections";

$string['name'] = "Name";
$string['endcorrection'] = "Correction";
$string['submission'] = "Submission";
$string['show'] = "show";
$string['nothing'] = "nothing";
$string['to'] = "to";
$string['topics'] = "Topics";
$string['overview'] = "Overview";
$string['noTopics'] = "No topics available";
$string['delete'] = "Delete";
$string['emptyEdgesAlert'] = 'Empty terms and/or edges!';
$string['emptyEdgesMessage'] = 'You have some edges and/or terms with no content. Please fill them before you leave.';

$string['source'] = 'Source';
$string['content'] = 'Content';
$string['target'] = 'Target';
$string['comment'] = 'Comment';
$string['corrected'] = 'Corrected';
$string['check'] = 'Check';

$string['username'] = 'Username';
$string['add'] = 'Add';
$string['content_correct'] = 'content';
$string['formal_correct'] = 'formal';

$string['editSubmitted'] = 'already submitted';

$string['status_active'] = 'active';
$string['status_corr_active'] = 'correction active';
$string['status_upcoming'] = 'topic upcoming';
$string['status_wait_submission'] = 'wait for submissions';
$string['status_i_corr_active'] = '{$a}. correction active';
$string['status_nothing_submitted'] = 'nothing submitted';
$string['status_completed'] = 'completed';
$string['status_undefined'] = 'undefined';
$string['status_corr_upcoming'] = 'correction upcoming';
$string['status_resubmit'] = 'Please resubmit';

$string['singleCorrection_status'] = 'status';
$string['singleCorrection_status_help'] = 'This is the correction of single connections. You can state here if a single connection is formal correct as well as content-based correct.';
$string['toOverview'] = 'Back to Overview';
