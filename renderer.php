<?php
# @Date:   2019-11-07T12:52:32+01:00
# @Last modified time: 2019-11-07T15:28:48+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_conceptmaps
 * @copyright 2018 RWTH Aachen (see README.md)
 * @authors   Rabea de Groot and Anna Heynkes
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
defined('MOODLE_INTERNAL') || die();

require_once('../../config.php');

class mod_conceptmaps_renderer extends plugin_renderer_base {

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_topic_overview($index) {
      return $this->render_from_template('conceptmaps/topic_overview', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_topic_overview_student($index) {
      return $this->render_from_template('conceptmaps/topic_overview_student', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_topic($index) {
      return $this->render_from_template('conceptmaps/topic', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_submissons($index) {
      return $this->render_from_template('conceptmaps/submissions', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_submissons_topic($index) {
      return $this->render_from_template('conceptmaps/submissionsTopic', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_autocorrection($index) {
      return $this->render_from_template('conceptmaps/autocorrection', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_conceptmap($index) {
      return $this->render_from_template('conceptmaps/conceptmap', $index->export_for_template($this));
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public function render_single_correction($index) {
      return $this->render_from_template('conceptmaps/single_correction', $index->export_for_template($this));
  }



  /**
   * Construct a tab header.
   *
   * @param moodle_url $baseurl
   * @param string $namekey
   * @param string $action
   * @param string $nameargs
   * @return tabobject
   */
  private function conceptmaps_create_tab(moodle_url $baseurl, $namekey = null, $action, $nameargs = null) {
      $taburl = new moodle_url($baseurl, array('action' => Action::get_string($action)));
      $tabname = get_string($namekey, 'conceptmaps', $nameargs);

      $id = $action;
      $tab = new tabobject($id, $taburl, $tabname);
      return $tab;
  }
  /**
   * Render the tab header hierarchy.
   *
   * @param moodle_url $baseurl
   * @param type $selected
   * @param type $context
   * @param type $inactive
   * @return type
   */
  public function conceptmaps_render_tabs(moodle_url $baseurl, $selected = null, $context, $inactive = null) {
      $level1 = array(
          $this->conceptmaps_create_tab($baseurl, 'topics_overview', Action::Topic_Overview),
          $this->conceptmaps_create_tab($baseurl, 'submissions', Action::Submissions),
          $this->conceptmaps_create_tab($baseurl, 'autocorrection', Action::Autocorrection)
      );
      return $this->tabtree($level1, $selected, $inactive);
  }
}
