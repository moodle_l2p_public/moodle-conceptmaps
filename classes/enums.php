<?php
# @Date:   2019-11-07T13:42:56+01:00
# @Last modified time: 2019-11-07T15:29:38+01:00

abstract class Action {
  const Topic_Overview = 0;
  const Submissions = 1;
  const Autocorrection = 2;
  const Groups = 3;
  const Help = 4;
  const Topic = 5;
  const CreateTopic = 6;
  const Delete = 7;
  const Edit = 8;
  const SubmissionsTopic = 9;
  const Submission = 10;
  const Solution = 11;
  const SingleCorrection = 12;
  const CorrectConceptmap = 13;

  public static function get_enum(string $action) {
    switch($action) {
      case "topic_overview": return Action::Topic_Overview;
      case "submissions": return Action::Submissions;
      case "autocorrection": return Action::Autocorrection;
      case "groups": return Action::Groups;
      case "help": return Action::Help;
      case "topic": return Action::Topic;
      case "createtopic": return Action::CreateTopic;
      case "delete": return Action::Delete;
      case "edit": return Action::Edit;
      case "submissionsTopic": return Action::SubmissionsTopic;
      case "submission": return Action::Submission;
      case "solution": return Action::Solution;
      case "singlecorrection": return Action::SingleCorrection;
      case "correctConceptmap": return Action::CorrectConceptmap;
      default: return Action::Topic_Overview;
    }
  }

  public static function get_string(int $enum) {
    switch($enum) {
      case Action::Topic_Overview: return "topic_overview";
      case Action::Submissions: return "submissions";
      case Action::Autocorrection: return "autocorrection";
      case Action::Groups: return "groups";
      case Action::Help: return "help";
      case Action::Topic: return "topic";
      case Action::CreateTopic: return "createtopic";
      case Action::Delete: return "delete";
      case Action::Edit: return "edit";
      case Action::SubmissionsTopic: return "submissionsTopic";
      case Action::Submission: return "submission";
      case Action::Solution: return "solution";
      case Action::SingleCorrection: return "singlecorrection";
      case Action::CorrectConceptmap: return "correctConceptmap";
      default: return "topic_overview";
    }
  }
}

abstract class Layout {
  const LayoutNone = 0;
  const LayoutFirst = 1;
  const LayoutEdit = 2;
  const LayoutSolution = 3;

  public static function get_enum(string $action) {
    switch($action) {
      case "layout_none": return Layout::LayoutNone;
      case "layout_first": return Layout::LayoutFirst;
      case "layout_edit": return Layout::LayoutEdit;
      case "layout_solution": return Layout::LayoutSolution;
      default: return Layout::LayoutNone;
    }
  }

  public static function get_string(int $enum) {
    switch($enum) {
      case Layout::LayoutNone: return "layout_none";
      case Layout::LayoutFirst: return "layout_first";
      case Layout::LayoutEdit: return "layout_edit";
      case Layout::LayoutSolution: return "layout_solution";
      default: return "layout_none";
    }
  }

}
