<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/lib/formslib.php");

class mod_conceptmaps_create_topic_form extends moodleform{

    //Add elements to form
    public function definition() {
        global $CFG;
        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('text', 'name', get_string('topic_name', 'conceptmaps'));
        $mform->setType('name', PARAM_NOTAGS);
        $mform->addRule('name', get_string('topic_name_missing', 'conceptmaps'), 'required', null, null, false, false);

        $mform->addElement('header', 'header_date', get_string('header_date', 'conceptmaps'));


        $mform->addElement('date_time_selector', 'start', get_string('topic_start', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('date_time_selector', 'end', get_string('topic_end', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('date_time_selector', 'startcorrection', get_string('topic_startcorrection', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('date_time_selector', 'endcorrection', get_string('topic_endcorrection', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('header', 'header_terms', get_string('header_terms', 'conceptmaps'));

        $mform->addElement('text', 'count_empty_terms', get_string('topic_term_empty_count', 'conceptmaps'));
        $mform->setType('count_empty_terms', PARAM_INT);
        $mform->addRule('count_empty_terms', get_string('topic_count_empty_terms_notnumeric', 'conceptmaps'), 'numeric', null, 'client', false, false);
        $mform->setDefault('count_empty_terms', 0);


        $repeatarray = array();
        $repeatarray[] = $mform->createElement('text', 'term', get_string('topic_term', 'conceptmaps', '{no}'));

        $repeatno = $this->_customdata['termcount'];
        $repeateloptions = array();
        $repeateloptions['term']['type'] = PARAM_NOTAGS;

        $this->repeat_elements($repeatarray, $repeatno,
                    $repeateloptions, 'term_repeats', 'term_add_fields', 2, null, true);

        $mform->addElement('hidden', 'cmid', 0);
        $mform->setType('cmid', PARAM_TEXT);

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_TEXT);

        if(isset($this->_customdata['topicid'])) {
          $mform->addElement('hidden', 'topic', 0);
          $mform->setType('topic', PARAM_TEXT);
          $mform->setDefault('topic', $this->_customdata['topicid']);
        }

        $this->add_action_buttons(true, $this->_customdata['submit_string']);
    }

}
