<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/lib/formslib.php");

class mod_conceptmaps_edit_topic_form extends moodleform{
    //Add elements to form
    public function definition() {
        global $CFG;

        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('header', 'header_date', get_string('header_date', 'conceptmaps'));


        $mform->addElement('date_time_selector', 'start', get_string('topic_start', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('date_time_selector', 'end', get_string('topic_end', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('date_time_selector', 'startcorrection', get_string('topic_startcorrection', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('date_time_selector', 'endcorrection', get_string('topic_endcorrection', 'conceptmaps'), array(
            'startyear' => 2015,
            'stopyear'  => 2099,
            'optional' => true
        ));

        $mform->addElement('hidden', 'cmid', 0);
        $mform->setType('cmid', PARAM_TEXT);

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_TEXT);

        $mform->addElement('hidden', 'action', 0);
        $mform->setType('action', PARAM_TEXT);

        $mform->addElement('hidden', 'topic', 0);
        $mform->setType('topic', PARAM_TEXT);

        $this->add_action_buttons(true, get_string('topic_edit_submit', 'conceptmaps'));
    }

}
