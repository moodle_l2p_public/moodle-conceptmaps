<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * The purpose of this script is to collect the output data for the template and
 * make it available to the renderer.
 */
class singleCorrection implements \renderable, \templatable {

  private $cmid;
  private $topicid;
  private $edges;
  private $perpage;

    /**
     * Constructor of renderable for autocorrection tab.
     * @param int $conceptmapsid Id of the conceptmaps instance
     */
    public function __construct($cmid, $topicid) {
      global $DB;
      $this->cmid = $cmid;
      $this->topicid = $topicid;
      $this->perpage = 5;
    }

    /**
     * This function is required by any renderer to retrieve the data structure
     * passed into the template.
     * @param \renderer_base $output
     * @return type
     */
    public function export_for_template(\renderer_base $output) {

      $data = new stdClass();
      $finished = optional_param('finished', 0, PARAM_BOOL);
      $data->table = $this->get_table($finished);
      $data->finished = $finished;
      $data->cmid = $this->cmid;
      $data->topicid = $this->topicid;

      return $data;
    }

    private function get_table($finished) {
      global $OUTPUT, $PAGE, $DB;
      $ret = "";
      $page = optional_param('page', 0, PARAM_INT);
      $this->perpage = optional_param('perpage', $this->perpage, PARAM_INT);
      $url = new moodle_url('/mod/conceptmaps/view.php', array('id' => $this->cmid, 'topic' => $this->topicid, "action" => Action::get_string(Action::SingleCorrection), "finished" => $finished, "page" => $page, "perpage" => $this->perpage));

      $count = $DB->count_records('conceptmaps_edges', ['conceptmapstopic' => $this->topicid, 'verified' => $finished]);

      $pagingbar = $OUTPUT->paging_bar($count, $page, $this->perpage, $url);
      $start = $page * $this->perpage;
      $this->edges = $DB->get_records_sql("SELECT e.id, s.name as 'source', t.name as 'target', e.userid, e.content, e.verification, e.verified, e.comment, e.version, e.auto_correction FROM mdl_conceptmaps_edges e INNER JOIN mdl_conceptmaps_student_terms s ON s.id = e.source INNER JOIN mdl_conceptmaps_student_terms t ON t.id = e.target WHERE e.conceptmapstopic = :topic AND e.verified = :finished ORDER BY id ASC", ['topic'=>$this->topicid, 'finished' => $finished], $start, $this->perpage);

      $html = '';

      $fulltable = new html_table();
      $fulltable->attributes['class'] = 'table table-striped ';
      $fulltable->attributes['class'] .= 'single-correction-table ';
      $fulltable->id = 'single-correction';
      $fulltable->summary = "Summary";
      $fulltable->head = [get_string('username', 'conceptmaps'), get_string('source', 'conceptmaps'), get_string('content', 'conceptmaps'), get_string('target', 'conceptmaps'), get_string('autocorrection', 'conceptmaps'), get_string('status', 'conceptmaps')." ".$OUTPUT->help_icon('singleCorrection_status', 'conceptmaps'), get_string('comment', 'conceptmaps'), get_string('corrected', 'conceptmaps')];
      $fulltable->data = $this->get_rows();
      $html .= html_writer::table($fulltable);

      $ret = $pagingbar;
      $ret .= $OUTPUT->container($html, 'correctionparent');

      return $ret;
    }

    private function get_rows() {
      $rows = [];

      // Body
      foreach ($this->edges as $key => $edge) {

        $row = new html_table_row();
        $row->id = 'edge_'.$edge->id;

        // cell name
        $username_cell = new html_table_cell();
        $username_cell->text = conceptmap_get_username($edge->userid);
        $row->cells[] = $username_cell;

        $source_cell = new html_table_cell();
        $source_cell->text = $edge->source;
        $row->cells[] = $source_cell;

        $content_cell = new html_table_cell();
        $content_cell->text = $edge->content;
        $row->cells[] = $content_cell;

        $target_cell = new html_table_cell();
        $target_cell->text = $edge->target;
        $row->cells[] = $target_cell;

        $json_edge = json_encode($edge);
        $autocorrection_cell = new html_table_cell();

        if($edge->auto_correction) {
          $autocorrection_symbol = "- ".get_string('delete', 'conceptmaps');
        } else {
          $autocorrection_symbol = "+ ".get_string('add', 'conceptmaps');
        }
        $autocorrection_cell->text = html_writer::tag('a', $autocorrection_symbol, array('id' => 'add_edge_'.$edge->id, 'href' => '#', 'data-cmid'=>$this->cmid));
        $row->cells[] = $autocorrection_cell;

        $status_cell = new html_table_cell();
        $status_cell->text = $this->get_status_cell($edge);
        $row->cells[] = $status_cell;

        $comment_cell = new html_table_cell();
        $comment_cell->text = html_writer::tag('input', null, array('class'=>'comment', 'id' => 'comment_'.$edge->id, 'data-cmid'=>$this->cmid, 'value' => $edge->comment));
        $row->cells[] = $comment_cell;

        $corrected_cell = new html_table_cell();
        $corrected_cell->text = $this->get_verified_cell($edge);
        $row->cells[] = $corrected_cell;

        $rows[] = $row;
      }

      return $rows;
    }

    private function get_verified_cell($edge) {

      $o = "";
      $o .= html_writer::start_tag('form', array('action' => "#", 'method' => 'post'));
      $o .= html_writer::tag('button', "check", array('id' => 'verified_'.$edge->id, 'data-cmid'=>$this->cmid ));
      $o .= html_writer::end_tag('form');
      return $o;
    }

    private function get_status_cell($edge) {
      if($edge->verification >= 2) {
        $formal_selected = true;
      } else {
        $formal_selected = false;
      }
      $formal_id = 'formal_correct_'.$edge->id;
      $formal_correct = html_writer::checkbox($formal_id, 2, $formal_selected, get_string('formal_correct', 'conceptmaps'), array('id'=>$formal_id, 'data-cmid' => $this->cmid));

      if($edge->verification == 1 || $edge->verification == 3) {
        $content_selected = true;
      } else {
        $content_selected = false;
      }

      $content_id =  'content_correct_'.$edge->id;
      $content_correct = html_writer::checkbox($content_id, 2, $content_selected, get_string('content_correct', 'conceptmaps'), array('id'=>$content_id, 'data-cmid' => $this->cmid ));

      return $formal_correct."<br>".$content_correct;
    }
}
