<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_conceptmaps.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Class to prepare a term for display.
 *
 * @package   mod_conceptmaps
 */
 class term extends \core\output\inplace_editable {
   public $id;
   public $name;
   public $conceptmapstopic;
   public $editable;
   public $positionx;
   public $positiony;
   public $originalterm;
   public $userid;
   public $version;

   /**
    * Constructor.
    *
    * @param stdClass $term
    */
   public function __construct($term) {
     global $DB;
     $this->id =$term->id;
     $this->name = $term->name;
     $this->conceptmapstopic = $term->conceptmapstopic;
     $this->editable = $term->editable;
     $this->positionx = $term->positionx;
     $this->positiony = $term->positiony;
     $this->originalterm = $term->id;
     $this->userid = $term->id;
     $this->version = $term->version;

     if($term->cmid) {
       $context = context_module::instance($term->cmid);
     } else {
       $id_of_conceptmaps_obj = $DB->get_record_sql("SELECT conceptmapsid FROM mdl_conceptmaps_topics WHERE id = :id", ["id" => $term->conceptmapstopic]);

       $id_of_conceptmaps = $id_of_conceptmaps_obj->conceptmapsid;
       $moduleinstance = $DB->get_record('conceptmaps', array('id' => $id_of_conceptmaps), '*', MUST_EXIST);
       $course         = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
       $cm             = get_coursemodule_from_instance('conceptmaps', $moduleinstance->id, $course->id, false, MUST_EXIST);
       $term->cmid = $cm->id;
       $context = context_module::instance($cm->id);
     }

     $capability = has_capability('mod/conceptmaps:onlystudent', $context);
     $editable = $capability && $this->editable;

     if($term->name === "") {
       $displayvalue = "<span style='font-style: italic;'>Empty</span>";
     } else {
       $displayvalue = "<span>" . $term->name . "</span>";
     }
     parent::__construct('mod_conceptmaps', 'conceptmaps_term', $term->id, $editable,
         $displayvalue,
         $term->name,
         new lang_string('editconceptmapterm', 'conceptmaps'),
         new lang_string('newtermfor', 'conceptmaps', $term->name));

   }

   /**
    * Updates conceptmaps term name and returns instance of this object
    *
    * @param int $termid
    * @param string $newvalue
    * @return static
    */
   public static function update($termid, $newvalue) {
       global $DB;
       $term = $DB->get_record('conceptmaps_student_terms', array('id' => $termid), '*', MUST_EXIST);
       //get Course_module
       $id_of_conceptmaps = $DB->get_records_sql("SELECT conceptmapsid FROM mdl_conceptmaps_topics WHERE id = :id", ["id" => $term->conceptmapstopic])["conceptmapsid"];
       $moduleinstance = $DB->get_record('conceptmaps', array('id' => $id_of_conceptmaps), '*', MUST_EXIST);
       $course         = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
       $cm             = get_coursemodule_from_instance('conceptmaps', $moduleinstance->id, $course->id, false, MUST_EXIST);
       $term->cmid = $cm->id;
       $context = context_module::instance($cm->id);
       \external_api::validate_context($context);
       require_capability('mod/conceptmaps:onlystudent', $context);
       $term->name = $newvalue;
       // Update record
       $DB->update_record("conceptmaps_student_terms", $term);

       return new static($term);
   }

   /**
    * Export this data so it can be used as the context for a mustache template (conceptmaps/term).
    *
    * @param renderer_base $output typically, the renderer that's calling this function
    * @return array data context for a mustache template
    */
   public function export_for_template(\renderer_base $output) {

     $parent_array = parent::export_for_template($output);

     $child_array = array(
       'id' => $this->id,
       'name' => $this->name,
       'conceptmapstopic' => $this->conceptmapstopic,
       'editable' => $this->editable,
       'positionx' => $this->positionx,
       'positiony' => $this->positiony,
       'originalterm' => $this->originalterm,
       'userid' => $this->userid,
       'version' => $this->version
     );

     return array_merge($parent_array, $child_array);
   }
 }
