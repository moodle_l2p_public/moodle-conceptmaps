<?php
# @Date:   2019-11-07T14:19:23+01:00
# @Last modified time: 2019-11-07T15:41:15+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class mod_conceptmaps_basic_controller
 *
 * @package     mod_conceptmaps
 * @copyright   2018 Sven Judel <sven.judel@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/mod/conceptmaps/classes/controller/basic_controller.php');
require_once($CFG->dirroot.'/mod/conceptmaps/classes/enums.php');

class mod_conceptmaps_view_student_controller extends mod_conceptmaps_basic_controller {

  /**
   * mod_conceptmaps_overview_student_controller constructor.
   * @param $cmid
   * @param $conceptmapsid
   */
  public function __construct($cmid, $conceptmapsid){
      parent::__construct($cmid, $conceptmapsid);
  }

  /**
   * Handle access to view_student
   */
  public function handle_access(){
      $context = context_module::instance($this->cmid);

      if(!has_capability('mod/conceptmaps:onlystudent', $context)){
          $redirectionTarget = new moodle_url('/login/index.php', array('id'=>$this->cmid));
          redirect($redirectionTarget->out());
      }
  }

  public function handle_action($action) {
    $ret = "";
    switch($action) {
      case Action::Topic_Overview:
        $ret .= $this->handle_topic_overview();
        return $ret;
      case Action::Edit:
        $topicid = required_param('topic', PARAM_INT);
        $ret .= $this->handle_edit($topicid);
        return $ret;
      case Action::Solution:
        $ret .= $this->handle_solution();
        return $ret;
      default:
        $ret .= $this->handle_topic_overview();
        return $ret;
    }
  }

  private function handle_topic_overview() {
    global $CFG;
    require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/topic_overview.php');
    return $this->myrenderer->render_topic_overview_student(new topic_overview($this->cmid, $this->conceptmapsid));
  }


  private function handle_edit($topicid) {
    global $USER;
    return $this->myrenderer->render_conceptmap(prepare_conceptmap($this->cmid, $this->conceptmapsid, $topicid, $USER->id, null, false));
  }


  private function handle_solution() {
    global $DB, $USER;
    $topicid = optional_param('topic', null, PARAM_INT);
    if($topicid == null) {
      $submission = required_param('submission', PARAM_INT);
      $submission_obj = $DB->get_record("conceptmaps_submissions", ['id' => $submission]);
      if($submission_obj->userid != $USER->id) {
        // User is not allowed to see this submission!
        $redirectionTarget = new moodle_url('/login/index.php', array('id'=>$this->cmid));
        redirect($redirectionTarget->out());
      }
      $topicid = $submission_obj->conceptmapstopic;
    } else {
      //get newest submission of this topic
      $submission_obj = array_values($DB->get_records('conceptmaps_submissions', ['conceptmapstopic' => $topicid, 'userid' => $USER->id, "submitted" => 1], "version DESC", "*", 0, 1))[0];
      $submission = $submission_obj->id;
    }


    return $this->myrenderer->render_conceptmap(prepare_conceptmap($this->cmid, $this->conceptmapsid, $topicid, $USER->id, $submission, true));
  }




  /**
   * Redirects to the main student view with a message about the successful upload of the
   * data
   */
  private function go_back_to_main_view(){
      $redirectionTarget = new moodle_url('/mod/conceptmaps/view_student.php', array('id'=>$this->cmid));
      redirect($redirectionTarget->out(), get_string('store_successful', 'conceptmaps'), null,
          \core\output\notification::NOTIFY_SUCCESS);
  }

}
