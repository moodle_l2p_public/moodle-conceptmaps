<?php
# @Date:   2019-11-07T14:19:23+01:00
# @Last modified time: 2019-11-07T15:33:49+01:00



// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class mod_conceptmaps_basic_controller
 *
 * @package     mod_conceptmaps
 * @copyright   2018 Sven Judel <sven.judel@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


abstract class mod_conceptmaps_basic_controller {

    /** @var int ID of course module*/
    protected $cmid;

    /** @var int ID of module instance*/
    protected $conceptmapsid;

    /** @var string The title of the page */
    protected $pageTitle = 'Title';

    /** @var mod_conceptmaps_renderer The renderer to use */
    protected $myrenderer = null;

    /**
     * mod_conceptmaps_basic_controller constructor.
     * @param $cmid
     * @param $conceptmapsid
     */
    public function __construct($cmid, $conceptmapsid){
      global $PAGE;
        $this->cmid = $cmid;
        $this->conceptmapsid = $conceptmapsid;
        $this->myrenderer = $PAGE->get_renderer('mod_conceptmaps');
    }

    /**
     * Returns the course module ID
     *
     * @return int
     */
    public function get_course_module_id(){
        return $this->cmid;
    }

    /**
     * Returns the module instance ID
     *
     * @return int
     */
    public function get_moduleinstance_id(){
        return $this->conceptmapsid;
    }

}
