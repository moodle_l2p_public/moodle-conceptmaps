<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_conceptmaps
 * @copyright   2019 LuFG I9 <rabea.degroot@rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function get_names_of_terms($terms) {
  $ret = array();
  foreach ($terms as $key => $value) {
    $ret[] = $value->name;
  }

  return $ret;
}

function delete_topic($id) {
  global $DB;
  //Delete topic
  $DB->delete_records('conceptmaps_topics', array('id' => $id));

  //Delete all Terms
  $DB->delete_records('conceptmaps_terms', array('conceptmapstopic' => $id));

  //TODO: delete all submissions etc
}

function get_topic($id) {
  global $DB;
  $topic = $DB->get_record('conceptmaps_topics', array('id' => $id));

  $topic->terms = array_values($DB->get_records('conceptmaps_terms', array('conceptmapstopic'=>$id)));

  return $topic;
}

function get_topic_user($topicid, $userid, $version){
  global $DB;
  $topic = $DB->get_record('conceptmaps_topics', array('id' => $topicid));

  $topic->terms = array_values($DB->get_records('conceptmaps_student_terms', array('conceptmapstopic'=>$topicid, 'userid'=>$userid, 'version'=>$version)));

  return $topic;
}

function get_edges_user($topicid, $userid, $version) {
  global $DB;

  return array_values($DB->get_records('conceptmaps_edges', array('conceptmapstopic'=>$topicid, 'userid'=>$userid, 'version'=>$version)));
}

function get_conceptmap_of_user($userid, $topic, $version) {
  global $DB;

  $map = $DB->get_record("conceptmaps_submissions", ["userid" => $userid, "conceptmapstopic" => $topic, "version" => $version]);

  return $map;
}

function get_newest_conceptmap_of_user($userid, $topic) {
  global $DB;
  $newestVersion = $DB->get_record("conceptmaps_submissions", ["userid" => $userid, "conceptmapstopic" => $topic], 'max(version) as version')->version;
  $map = get_conceptmap_of_user($userid, $topic, $newestVersion);
  return $map;
}

/**
 * Get the name of the user.
 * @param int $userid Id of the user
 * @return string username
 */
function conceptmap_get_username($userid) {
    global $DB;
    $user = $DB->get_record('user', array('id' => $userid));
    return fullname($user);
}

function prepare_conceptmap($cmid, $conceptmapsid, $topicid, $userid, $submissionid = null, $doCorrection = false) {
  global $CFG, $PAGE;
  require_once($CFG->dirroot . '/mod/conceptmaps/classes/output/conceptmap.php');

  $stringman = get_string_manager();
  // With this method you get the strings of the language-Files.
  $strings = $stringman->load_component_strings('conceptmaps', 'en');
  // Method to use the language-strings in javascript.
  $PAGE->requires->strings_for_js(array_keys($strings), 'conceptmaps');

  $PAGE->requires->js("/mod/conceptmaps/js/jquery.ui.touch-punch-0.2.2.min.js");
  $PAGE->requires->js("/mod/conceptmaps/js/dom.jsPlumb-1.7.6.js");
  $PAGE->requires->js("/mod/conceptmaps/js/conceptmap.js");

  if($submissionid == null) {
    $conceptmap_from_db = get_newest_conceptmap_of_user($userid, $topicid);
    if($conceptmap_from_db != null) {
      if($conceptmap_from_db->submitted == 1) {
        if($conceptmap_from_db->failed == 1) {
          //create new version
          $submissionid = null;
        } else {
          // ERROR! The conceptmap is already submitted should not be able to be edited!
          // TODO: Throw error, user should not be in this situation
          throw new \Exception("Error: Not Allowed", 1);
        }
      } else {
        $submissionid = $conceptmap_from_db->id;
      }
    }else {
      $submissionid = null;

    }
    $conceptmap = new conceptmap($cmid, $conceptmapsid, $topicid, $submissionid, false);
    if($conceptmap->new && $conceptmap->version == 1) {
      $layout = Layout::LayoutFirst;
    } else {
      $layout = Layout::LayoutEdit;
    }
    $newVersion = true; //TODO

  } else {
    $conceptmap = new conceptmap($cmid, $conceptmapsid, $topicid, $submissionid, $doCorrection);
    $newVersion = false;
    $layout = Layout::LayoutSolution;
  }

  $PAGE->requires->js_init_call('startConceptmaps', array($cmid, $topicid, $conceptmap->topic->name, $conceptmap->id, $layout, $conceptmap->version, json_encode($conceptmap->topic->terms), json_encode(edges_for_JS($conceptmap->edges)), $newVersion,  $doCorrection, false));

  return $conceptmap;
}

function edges_for_JS($edges) {
  $ret = [];
  foreach ($edges as $key => $edge) {
    $retedge = new stdClass();
    $retedge->connectionid = $edge->connectionid;
    $retedge->sourceid = $edge->source;
    $retedge->targetid = $edge->target;
    $retedge->content = $edge->content;
    $retedge->verification = $edge->verification;
    $retedge->comment = $edge->comment;
    $ret[] = $retedge;
  }
  return $ret;
}

function conceptmaps_change_auto_edge($id) {
  return true;
}

function conceptmaps_update_edges_of_auto($auto_edge, bool $change_auto) {
  global $DB;

  if($change_auto) {
    $DB->update_record('conceptmaps_auto_edges', $auto_edge);
  }
  try{
    $transaction = $DB->start_delegated_transaction();
    $edges_ids = $DB->get_records('conceptmaps_edges', ["auto_correction" => $auto_edge->id, "verified" => 0], '', 'id');

    foreach ($edges_ids as $key => $value) {
      $update_obj = new stdClass();
      $update_obj->id = $value->id;
      $update_obj->verification = $auto_edge->verification;
      $update_obj->comment = $auto_edge->comment;

      $DB->update_record('conceptmaps_edges', $update_obj);
    }
    $transaction->allow_commit();
    return $edges_ids;
  } catch(Exception $e) {
      $transaction->rollback($e);
      return false;
  }
}
